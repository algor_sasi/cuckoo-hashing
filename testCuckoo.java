public class testCuckoo {
    public static void main(String[] args) {
        CuckooHash cuckoo = new CuckooHash(7);
        cuckoo.put(13, "AA");

        printTable(cuckoo);
        System.out.println("------------------------------");

        cuckoo.put(7, "BB");
        cuckoo.put(3, "CC");
        cuckoo.put(11, "DD");
        
        printTable(cuckoo);
        System.out.println("------------------------------");

        System.out.println(cuckoo.remove(3));
    }

    private static void printTable(CuckooHash cuckoo) {
        for (int i = 1; i < cuckoo.getNumberOfTable() + 1; i++) {
            for (int j = 0; j < cuckoo.getN(); j++) {
                if (cuckoo.getT()[i - 1][j] != null) {
                    System.out.println("[" + i + "," + j + "] = " + cuckoo.getT()[i - 1][j].key + " , " + cuckoo.getT()[i - 1][j].value);
                } else {
                    System.out.println("[" + i + "," + j + "] = is null");
                }
                System.out.println("");
            }
        }
    }

}
