public class CuckooHash {
    int N;
    final int numberOfTable = 2;
    Node[][] T;
    int cntNode = 0;

    public CuckooHash(int size) {
        this.N = size;
        T = new Node[numberOfTable][N];
        for (int i = 0; i < N; i++) {
            for (int j = 0; j < numberOfTable; j++) {
                T[j][i] = null;
            }
        }
    }

    public int getN() {
        return this.N;
    }

    public int getNumberOfTable() {
        return this.numberOfTable;
    }

    public Node[][] getT() {
        return this.T;
    }

    private int getHashIndex(int tableCase, int key) {
        switch (tableCase) {
            case 1 : {
                return key % N;
            }
            case 2 : {
                return (key / N) % N;
            }
        }
        return 0;
    }

    public String get(int key) {
        if (T[0][getHashIndex(1, key)] != null && T[0][getHashIndex(1, key)].key == key) {
            return T[0][getHashIndex(1, key)].value;
        }
        if (T[1][getHashIndex(2, key)] != null && T[1][getHashIndex(2, key)].key == key) {
            return T[1][getHashIndex(2, key)].value;
        }
        return null;
    }

    public Node remove(int key) {
        Node temp;
        if (T[0][getHashIndex(1, key)] != null && T[0][getHashIndex(1, key)].key == key) {
            temp = T[0][getHashIndex(1, key)];
            T[0][getHashIndex(1, key)] = null;
            return temp;
        }
        if (T[1][getHashIndex(2, key)] != null && T[1][getHashIndex(2, key)].key == key) {
            temp = T[1][getHashIndex(2, key)];
            T[1][getHashIndex(2, key)] = null;
            return temp;
        }
        return null;
    }

    public void put(int key, String value) {
        if (T[0][getHashIndex(1, key)] != null && T[0][getHashIndex(1, key)].key == key) {
            T[0][getHashIndex(1, key)].value = value;
            return;
        }
        if (T[1][getHashIndex(2, key)] != null && T[1][getHashIndex(2, key)].key == key) {
            T[1][getHashIndex(2, key)].value = value;
            return;
        }
    }

}